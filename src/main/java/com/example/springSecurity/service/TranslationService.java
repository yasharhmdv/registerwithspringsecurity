package com.example.springSecurity.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class TranslationService {

    private final MessageSource messageSource;
    public String findByKey(String key, String lang, Objects... arguments){
        try {
            return messageSource.getMessage(key, arguments, new Locale(lang,lang));
        }catch (NoSuchMessageException e){
            return key;
        }
    }
}
