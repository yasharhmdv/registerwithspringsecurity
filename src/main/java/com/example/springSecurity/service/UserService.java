package com.example.springSecurity.service;

import com.example.springSecurity.config.security.JwtService;
import com.example.springSecurity.dto.LoginResponseDto;
import com.example.springSecurity.dto.RegisterDto;
import com.example.springSecurity.enums.AuthorityEnums;
import com.example.springSecurity.enums.ErrorCodes;
import com.example.springSecurity.exception.BadRequestException;
import com.example.springSecurity.model.Authority;
import com.example.springSecurity.model.User;
import com.example.springSecurity.repository.AuthorityRepository;
import com.example.springSecurity.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final AuthorityRepository authorityRepository;
    private final JwtService jwtService;


    public LoginResponseDto register(RegisterDto registerDto) {
        Optional<User> user = Optional.ofNullable(userRepository.findByUsername(registerDto.getUsername()));
        Optional<User> userMail = (userRepository.findByEmail(registerDto.getEmail()));
        if (user.isPresent()){
            throw new BadRequestException(ErrorCodes.USER_ALREADY_PRESENT);
        }
        if (!userMail.isPresent()){
            throw new BadRequestException(ErrorCodes.MAIL_ALREADY_USED);
        }
        Authority authority = authorityRepository.findByAuthority(AuthorityEnums.USER)
                .orElseThrow(() -> new BadRequestException(ErrorCodes.AUTHORITY_NOT_FOUND));
        if (!registerDto.getPassword().equals(registerDto.getRepeatPassword())){
            throw new BadRequestException(ErrorCodes.PASSWORD_DIDNT_MATCH);
        }
        User buildUser = User.builder()
                .authorities(List.of(authority))
                .password(bCryptPasswordEncoder.encode(registerDto.getPassword()))
                .username(registerDto.getUsername())
                .email(registerDto.getEmail())
                .build();
        userRepository.save(buildUser);
        return  LoginResponseDto.builder()
                .jwt(jwtService.issueToken(buildUser))
                .build();

    }
}
