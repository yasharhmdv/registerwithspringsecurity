package com.example.springSecurity.repository;

import com.example.springSecurity.enums.AuthorityEnums;
import com.example.springSecurity.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {

    Optional<Authority> findByAuthority(AuthorityEnums authorityEnums);
}
