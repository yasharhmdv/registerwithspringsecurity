package com.example.springSecurity.controller;

import com.example.springSecurity.dto.LoginDto;
import com.example.springSecurity.dto.LoginResponseDto;
import com.example.springSecurity.dto.RegisterDto;
import com.example.springSecurity.dto.UserDto;
import com.example.springSecurity.enums.ErrorCodes;
import com.example.springSecurity.exception.BadRequestException;
import com.example.springSecurity.repository.UserRepository;
import com.example.springSecurity.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final UserService userService;
    private final UserRepository userRepository;



    @PostMapping("/register")
    public ResponseEntity<LoginResponseDto> register(@RequestBody @Valid RegisterDto registerDto) {
        if (userRepository.existsByEmail(registerDto.getEmail())){
            throw new BadRequestException(ErrorCodes.MAIL_ALREADY_USED);
        }
        return ResponseEntity.ok(userService.register(registerDto));

    }


}
