package com.example.springSecurity.controller;

import com.example.springSecurity.model.User;
import com.example.springSecurity.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/hello")
public class HelloController {

    @GetMapping
    public ResponseEntity<String> helloGet() {
        return ResponseEntity.ok("Hello world from Spring");
    }




}
