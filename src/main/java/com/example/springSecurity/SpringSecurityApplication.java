package com.example.springSecurity;

import com.example.springSecurity.model.Authority;
import com.example.springSecurity.model.User;
import com.example.springSecurity.repository.AuthorityRepository;
import com.example.springSecurity.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class SpringSecurityApplication implements CommandLineRunner {
	private final UserRepository userRepository;
	private final AuthorityRepository authorityRepository;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
//		Authority user = Authority.builder()
//				.authority("USER")
//				.build();
//		Authority admin = Authority.builder()
//				.authority("ADMIN")
//				.build();
//		authorityRepository.save(user);
//		authorityRepository.save(admin);
//
//		User user1 = User.builder()
//				.username("Yashar")
//				.email("@mail.com")
//				.password(bCryptPasswordEncoder.encode("12345"))
//				.authorities(List.of(user))
//						.build();
//		User admin2 = User.builder()
//				.username("Admin")
//				.email("@mail.com")
//				.password(bCryptPasswordEncoder.encode("12345"))
//				.authorities(List.of(admin))
//				.build();
//
//		userRepository.save(user1);
//		userRepository.save(admin2);
	}
}
