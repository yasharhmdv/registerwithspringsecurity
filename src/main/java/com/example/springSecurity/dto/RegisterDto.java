package com.example.springSecurity.dto;

import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class RegisterDto {

    String username;
    String email;
    @Size(min=4, max = 10)
    String password;
    @Size(min=4, max = 10)
    String repeatPassword;

}
