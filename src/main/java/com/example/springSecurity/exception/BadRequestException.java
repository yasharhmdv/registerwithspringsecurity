package com.example.springSecurity.exception;


import com.example.springSecurity.enums.ErrorCodes;
import lombok.Data;

@Data
public class BadRequestException extends RuntimeException{

    private final ErrorCodes errorCodes;

    public BadRequestException(ErrorCodes errorCodes){

        this.errorCodes = errorCodes;

    }
}
