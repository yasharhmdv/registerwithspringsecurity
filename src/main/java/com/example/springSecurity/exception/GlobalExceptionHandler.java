package com.example.springSecurity.exception;

import com.example.springSecurity.service.TranslationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

import static org.springframework.http.HttpHeaders.ACCEPT_LANGUAGE;

@ControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {
    private final TranslationService translationService;

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ErrorResponseDto> handleBadRequestException(BadRequestException e, WebRequest webRequest) {
        var lang = webRequest.getHeader(ACCEPT_LANGUAGE)== null ? "en": webRequest.getHeader(ACCEPT_LANGUAGE);
        e.printStackTrace();
        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .details(translationService.findByKey(e.getErrorCodes().name(),lang))
                .build());
    }
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponseDto> handleMethodArgumentNotValidException(MethodArgumentNotValidException e, WebRequest webRequest) {
        e.printStackTrace();
        ErrorResponseDto responseDto = ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .build();
        e.getBindingResult()
                .getFieldErrors()
                .stream()
                .forEach(error ->{
                    Map<String,String> data = responseDto.data;
                    data.put(error.getField(), error.getDefaultMessage());
                });
        return ResponseEntity.status(400).body(responseDto);

    }

}
