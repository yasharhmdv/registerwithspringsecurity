package com.example.springSecurity.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
public class ErrorResponseDto {

    int status;
    String title;
    String details;
    @Builder.Default
    Map<String, String> data = new HashMap<>();
}
