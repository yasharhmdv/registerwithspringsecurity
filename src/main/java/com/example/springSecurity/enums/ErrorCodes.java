package com.example.springSecurity.enums;

public enum ErrorCodes {
    USER_ALREADY_PRESENT,
    PASSWORD_DIDNT_MATCH,
    AUTHORITY_NOT_FOUND,
    USER_NOT_FOUND,
    MAIL_ALREADY_USED
}
