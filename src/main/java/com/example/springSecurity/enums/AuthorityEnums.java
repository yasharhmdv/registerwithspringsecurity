package com.example.springSecurity.enums;

public enum AuthorityEnums {
    USER,
    ADMIN
}
